import { Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from './entities/product.entity';
import { Repository } from 'typeorm';
import { ProductNotFoundException } from './exception/productNotFound.exception';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private readonly productRepository: Repository<Product>,
  ) {}

  async create(product: CreateProductDto) {
    const newProduct = await this.productRepository.create(product);
    if (newProduct) {
      await this.productRepository.save(newProduct);
      return newProduct;
    }
    throw new Error('Unable to create a product');
  }

  async findAll() {
    const products = await this.productRepository.find({ relations: ['orders'] });
    if (products) return products;
    throw new Error('Unable to get products');
  }

  async findOne(id: number) {
    const product = this.productRepository.findOne(id, { relations: ['orders'] });
    if (product) return product;
    throw new ProductNotFoundException(id);
  }

  async update(id: number, product: UpdateProductDto) {
    await this.productRepository.update(id, product);
    const updatedProduct = await this.productRepository.findOne(id);
    if (updatedProduct) return updatedProduct;
    throw new ProductNotFoundException(id);
  }

  async remove(id: number) {
    const product = await this.productRepository.findOne(id);
    if (product) {
      await this.productRepository.remove(product);
      return product;
    }
    throw new ProductNotFoundException(id);
  }
}
