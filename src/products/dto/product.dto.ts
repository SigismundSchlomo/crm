export class ProductDto {
  public id: number;
  public photoUrl?: string;
  public leftNum: number;
  public price: number;
}