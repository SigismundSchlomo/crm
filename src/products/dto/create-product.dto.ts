import { PartialType } from '@nestjs/mapped-types';
import { ProductDto } from './product.dto';
import { IsNotEmpty, IsNumber, IsOptional, IsString, IsUrl } from 'class-validator';

export class CreateProductDto extends PartialType(ProductDto){

  @IsUrl()
  @IsNotEmpty()
  @IsOptional()
  public photoUrl?: string;

  @IsNumber()
  @IsOptional()
  public leftNum?: number;

  @IsNumber()
  public price: number;
}
