import { PartialType } from '@nestjs/mapped-types';
import { ProductDto } from './product.dto';
import { IsNumber, IsOptional, IsUrl } from 'class-validator';

export class UpdateProductDto extends PartialType(ProductDto) {

  @IsUrl()
  @IsOptional()
  public photoUrl?: string;

  @IsNumber()
  @IsOptional()
  public leftNum?: number;

  @IsNumber()
  @IsOptional()
  public price?: number;
}
