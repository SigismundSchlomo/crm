import { Test, TestingModule } from '@nestjs/testing';
import { ProductsService } from '../products.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Product } from '../entities/product.entity';
import { ProductNotFoundException } from '../exception/productNotFound.exception';
import { productMock } from '../../utils/mocks/product.mock';

describe('ProductsService', () => {
  let service: ProductsService;
  const create = jest.fn();
  const save = jest.fn();
  const find = jest.fn();
  const findOne = jest.fn();
  const update = jest.fn();
  const deleteFunc = jest.fn();

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ProductsService,
        {
          provide: getRepositoryToken(Product),
          useValue: {
            create,
            save,
            find,
            findOne,
            update,
            remove: deleteFunc,
          },
        },
      ],
    }).compile();

    service = module.get<ProductsService>(ProductsService);
  });

  describe('when creating a product', () => {
    describe('and it succeed', () => {
      beforeEach(() => {
        create.mockReturnValue(productMock);
        save.mockReturnValue(productMock);
      });
      it('should return a product', () => {
        expect(service.create(productMock)).resolves.toEqual(productMock);
      });
    });

    describe('and it fails', () => {
      beforeEach(() => {
        create.mockReturnValue(undefined);
        save.mockReturnValue(undefined);
      });
      it('should throw an error', () => {
        expect(service.create(productMock)).rejects.toThrow(Error);
      });
    });
  });

  describe('when getting all products', () => {
    describe('and it succeed', () => {
      const arrayMock = [productMock];
      beforeEach(() => {
        find.mockReturnValue(arrayMock);
      });
      it('should return an array of products', () => {
        expect(service.findAll()).resolves.toEqual(arrayMock);
      });
    });

    describe('and it fails', () => {
      beforeEach(() => {
        find.mockReturnValue(undefined);
      });
      it('should throw an error', () => {
        expect(service.findAll()).rejects.toThrow(Error);
      });
    });
  });

  describe('when getting one product', () => {
    describe('and it succeed', () => {
      beforeEach(() => {
        findOne.mockReturnValue(productMock);
      });
      it('should return a product', () => {
        const id = 1;
        expect(service.findOne(id)).resolves.toEqual(productMock);
      });
    });
    describe('and it fails', () => {
      beforeEach(() => {
        findOne.mockReturnValue(undefined);
      });
      it('should throw an error', () => {
        const id = 1;
        expect(service.findOne(id)).rejects.toThrow(ProductNotFoundException);
      });
    });
  });

  describe('when updating a product', () => {
    describe('and it succeed', () => {
      beforeEach(() => {
        update.mockReturnValue(productMock);
        findOne.mockReturnValue(productMock);
      });
      it('should return a product', () => {
        const id = 1;
        expect(service.update(id, productMock)).resolves.toEqual(productMock);
      });
    });

    describe('and it fails', () => {
      beforeEach(() => {
        update.mockReturnValue(undefined);
        findOne.mockReturnValue(undefined);
      });
      it('should throw an error', () => {
        const id = 1;
        expect(service.update(id, productMock)).rejects.toThrow(ProductNotFoundException);
      });
    });
  });

  describe('when deleting a product', () => {
    describe('and it succeed', () => {
      beforeEach(() => {
        findOne.mockReturnValue(productMock);
        deleteFunc.mockReturnValue(productMock);
      });
      it('should return a product', () => {
        const id = 1;
        expect(service.remove(id)).resolves.toEqual(productMock);
      });
    });

    describe('and it fails', () => {
      beforeEach(() => {
        findOne.mockReturnValue(undefined);
        deleteFunc.mockReturnValue(undefined);
      });
      it('should throw an error', () => {
        const id = 1;
        expect(service.remove(id)).rejects.toThrow(ProductNotFoundException);
      });
    });
  });
});
