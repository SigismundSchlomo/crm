import { Test, TestingModule } from '@nestjs/testing';
import { ProductsController } from '../products.controller';
import { ProductsService } from '../products.service';
import { productMock } from '../../utils/mocks/product.mock';

describe('ProductsController', () => {
  let controller: ProductsController;
  const create = jest.fn();
  const findAll = jest.fn();
  const findOne = jest.fn();
  const update = jest.fn();
  const remove = jest.fn();

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProductsController],
      providers: [
        {
          provide: ProductsService,
          useValue: {
            create,
            findAll,
            findOne,
            update,
            remove,
          },
        },
      ],
    }).compile();

    controller = module.get<ProductsController>(ProductsController);
  });

  describe('when creating a product', () => {
    beforeEach(() => {
      create.mockReturnValue(productMock);
    });
    it('should return a product', () => {
      expect(controller.create(productMock)).toEqual(productMock);
    });
  });

  describe('when getting all product', () => {
    const arrayMock = [productMock];
    beforeEach(() => {
      findAll.mockReturnValue(arrayMock);
    });
    it('should return an array of products', () => {
      expect(controller.findAll()).toEqual(arrayMock);
    });
  });

  describe('when getting one product', () => {
    beforeEach(() => {
      findOne.mockReturnValue(productMock);
    });
    it('should return a product', () => {
      const id = 1;
      expect(controller.findOne(id)).toEqual(productMock);
    });
  });

  describe('when updating a product', () => {
    beforeEach(() => {
      update.mockReturnValue(productMock);
    });
    it('should return a product', () => {
      const id = 1;
      expect(controller.update(id, productMock)).toEqual(productMock);
    });
  });

  describe('when deleting a product', () => {
    beforeEach(() => {
      remove.mockReturnValue(productMock);
    });
    it('should return a product', () => {
      const id = 1;
      expect(controller.remove(id)).toEqual(productMock);
    });
  });
});
