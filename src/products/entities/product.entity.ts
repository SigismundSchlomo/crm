import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Order } from '../../orders/entities/order.entity';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ nullable: true })
  public photoUrl?: string;

  @Column()
  public leftNum: number;

  @Column({ type: 'text' })
  public description: string;

  @Column()
  public price: number;

  @ManyToMany(() => Order, (order: Order) => order.orderedProducts)
  public orders: Order[];
}
