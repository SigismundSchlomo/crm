import { Test, TestingModule } from '@nestjs/testing';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { userMock } from '../utils/mocks/user.mock';

describe('UsersController', () => {
  let controller: UsersController;
  const create = jest.fn();

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        {
          provide: UsersService,
          useValue: {
            create,
          },
        },
      ],
    }).compile();

    controller = module.get<UsersController>(UsersController);
  });

  describe('when creating user', () => {
    beforeEach(() => {
      create.mockReturnValue(userMock);
    });
    it('should return a client', () => {
      expect(controller.create(userMock)).toEqual(userMock)
    })
  });
});
