import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from './users.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { UserNotFoundException } from './exceptions/UserNotFoundException';
import { userMock } from '../utils/mocks/user.mock';

describe('UsersService', () => {
  let usersService: UsersService;
  const findOne = jest.fn();
  const create = jest.fn();
  const save = jest.fn();

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        {
          provide: getRepositoryToken(User),
          useValue: {
            findOne,
            create,
            save,
          },
        },
      ],
    }).compile();

    usersService = await module.get(UsersService);
  });

  describe('when getting user by email', () => {
    describe('and it succeed', () => {
      beforeEach(() => {
        findOne.mockReturnValue(userMock);
      });
      it('should return a User', () => {
        const email = 'test@test.com';
        expect(usersService.getByEmail(email)).resolves.toEqual(userMock);
      });
    });

    describe('and it fails', () => {
      beforeEach(() => {
        findOne.mockReturnValue(undefined);
      });
      it('should throw a NotFound exception', () => {
        const email = 'test@test.com';
        expect(usersService.getByEmail(email)).rejects.toThrow(UserNotFoundException);
      });
    });
  });

  describe('when creating a user', () => {
    describe('and it succeed', () => {
      beforeEach(() => {
        create.mockReturnValue(userMock);
        save.mockReturnValue(userMock);
      });
      it('should return a new User', () => {
        expect(usersService.create(userMock)).resolves.toEqual(userMock);
      });
    });

    describe('and it fails', () => {
      beforeEach(() => {
        create.mockReturnValue(undefined);
        save.mockReturnValue(undefined);
      });
      it('should throw an Error', () => {
        expect(usersService.create(userMock)).rejects.toThrow(Error);
      });
    });
  });

  describe('when getting user by id', () => {
    describe('and it succeed', () => {
      beforeEach(() => {
        findOne.mockReturnValue(userMock);
      });
      it('should return a User', () => {
        const id = 1;
        expect(usersService.getById(id)).resolves.toEqual(userMock);
      });
    });

    describe('and it fails', () => {
      beforeEach(() => {
        findOne.mockReturnValue(undefined);
      });
      it('should throw a NotFound error', () => {
        const id = 1;
        expect(usersService.getById(id)).rejects.toThrow(UserNotFoundException);
      });
    });
  });
});
