import { HttpException, HttpStatus } from '@nestjs/common';
import { string } from '@hapi/joi';

export class UserNotFoundException extends HttpException {
  constructor(identifier: string | number) {
    if (typeof identifier === 'string') {
      super(`User with email: ${identifier} is not found`, HttpStatus.NOT_FOUND);
    } else if (typeof identifier === 'number') {
      super(`User with id: ${identifier} is not found`, HttpStatus.NOT_FOUND);
    }
  }
}
