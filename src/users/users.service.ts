import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';
import { UserNotFoundException } from './exceptions/UserNotFoundException';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  async getByEmail(email: string) {
    const user = await this.usersRepository.findOne({ email });
    if (user) return user;
    throw new UserNotFoundException(email);
  }

  async create(userData: CreateUserDto) {
    const newUser = await this.usersRepository.create(userData);
    if (newUser) {
      await this.usersRepository.save(newUser);
      return newUser;
    }
    throw new Error();
  }

  async getById(id: number) {
    const user = await this.usersRepository.findOne({ id });
    if (user) return user;
    throw new UserNotFoundException(id);
  }
}
