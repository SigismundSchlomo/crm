import { AuthenticationService } from './authentication.service';
import { UsersService } from '../users/users.service';
import { User } from '../users/entities/user.entity';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { mockedConfigService } from '../utils/mocks/config.mock';

describe('The AuthenticationService', () => {
  let authenticationService: AuthenticationService;
  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [
        UsersService,
        AuthenticationService,
        {
          provide: ConfigService,
          useValue: mockedConfigService,
        },
        {
          provide: JwtService,
          useFactory: () => ({
            sign: jest.fn(() => ''),
          }),
        },
        {
          provide: getRepositoryToken(User),
          useValue: {},
        },
      ],
    }).compile();
    const usersService = await module.get(UsersService);
    const configService = await module.get(ConfigService);
    const jwt = await module.get(JwtService);
    authenticationService = new AuthenticationService(usersService, jwt, configService);
  });
  describe('when creating a cookie', () => {
    it('should return a string', () => {
      const userId = 1;
      expect(typeof authenticationService.getCookieWithJwtToken(userId)).toEqual('string');
    });
  });
});
