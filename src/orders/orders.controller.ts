import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, Req } from '@nestjs/common';
import { OrdersService } from './orders.service';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import JwtAuthenticationGuard from '../authentication/jwt-authentication.guard';
import { ClientsService } from '../clients/clients.service';

@Controller('orders')
export class OrdersController {
  constructor(
    private readonly ordersService: OrdersService,
    private readonly clientsService: ClientsService,
  ) {}

  @Post()
  @UseGuards(JwtAuthenticationGuard)
  async create(@Body() order: CreateOrderDto, clientId: number) {
    const client = await this.clientsService.getById(clientId);
    return this.ordersService.create(order, client);
  }

  @Get()
  findAll() {
    return this.ordersService.getAll();
  }

  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.ordersService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: number, @Body() updateOrderDto: UpdateOrderDto) {
    return this.ordersService.update(+id, updateOrderDto);
  }

  @Delete(':id')
  remove(@Param('id') id: number) {
    return this.ordersService.remove(+id);
  }
}
