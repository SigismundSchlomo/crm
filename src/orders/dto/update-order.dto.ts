import { PartialType } from '@nestjs/mapped-types';
import { OrderDto } from './order.dto';
import { IsDate, IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';

export class UpdateOrderDto extends PartialType(OrderDto) {

  @IsDate()
  @IsNotEmpty()
  @IsOptional()
  public date?: Date;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  public deliveryAddress?: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  public additionalNote?: string;

  @IsNumber()
  @IsNotEmpty()
  @IsOptional()
  public totalPrice?: number;
}
