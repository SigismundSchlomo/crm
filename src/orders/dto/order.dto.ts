export class OrderDto {
  public id: number;
  public date: Date;
  public deliveryAddress: string;
  public additionalNote?: string;
  public totalPrice: number;
}