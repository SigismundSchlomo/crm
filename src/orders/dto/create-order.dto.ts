import { PartialType } from '@nestjs/mapped-types';
import { OrderDto } from './order.dto';
import { IsDate, IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateOrderDto extends PartialType(OrderDto){

  @IsDate()
  @IsNotEmpty()
  public date: Date;

  @IsString()
  @IsNotEmpty()
  public deliveryAddress: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  public additionalNote?: string;

  @IsNumber()
  @IsNotEmpty()
  public totalPrice: number;
}
