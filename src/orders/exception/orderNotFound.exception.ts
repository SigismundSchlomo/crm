import { HttpException, HttpStatus } from '@nestjs/common';

export class OrderNotFoundException extends HttpException {
  constructor(orderId: number) {
    super(`Order with id ${orderId} not found`, HttpStatus.NOT_FOUND);
  }
}
