import { Test, TestingModule } from '@nestjs/testing';
import { OrdersService } from '../orders.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Order } from '../entities/order.entity';
import { clientMock } from '../../utils/mocks/client.mock';
import { OrderNotFoundException } from '../exception/orderNotFound.exception';
import { orderMock } from '../../utils/mocks/order.mock';

describe('OrdersService', () => {
  let service: OrdersService;
  const create = jest.fn();
  const save = jest.fn();
  const find = jest.fn();
  const findOne = jest.fn();
  const update = jest.fn();
  const deleteFunc = jest.fn();

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        OrdersService,
        {
          provide: getRepositoryToken(Order),
          useValue: {
            create,
            save,
            find,
            findOne,
            update,
            remove: deleteFunc,
          },
        },
      ],
    }).compile();

    service = module.get<OrdersService>(OrdersService);
  });

  describe('when creating an Order', () => {
    describe('and it succeed', () => {
      const order = {
        ...orderMock,
        client: clientMock,
      };
      beforeEach(() => {
        create.mockReturnValue(order);
        save.mockReturnValue(order);
      });
      it('should return a new Order', () => {
        expect(service.create(orderMock, clientMock)).resolves.toEqual(order);
      });
    });

    describe('and it fails', () => {
      beforeEach(() => {
        create.mockReturnValue(undefined);
        save.mockReturnValue(undefined);
      });
      it('should throw an error', () => {
        expect(service.create(orderMock, clientMock)).rejects.toThrow(Error);
      });
    });
  });

  describe('when getting all orders', () => {
    describe('and it succeed', () => {
      const order = {
        ...orderMock,
        client: clientMock,
      };
      const mockedArray = [order];
      beforeEach(() => {
        find.mockReturnValue(mockedArray);
      });
      it('should return an array of orders', () => {
        expect(service.getAll()).resolves.toEqual(mockedArray);
      });
    });

    describe('and it fails', () => {
      beforeEach(() => {
        find.mockReturnValue(undefined);
      });
      it('should throw an error', () => {
        expect(service.getAll()).rejects.toThrow(Error);
      });
    });
  });

  describe('when getting an order by id', () => {
    describe('and it succeed', () => {
      const order = {
        ...orderMock,
      };
      beforeEach(() => {
        findOne.mockReturnValue(order);
      });
      it('should return an order', () => {
        const id = 1;
        expect(service.findOne(id)).resolves.toStrictEqual(order);
      });
    });

    describe('and it fails', () => {
      beforeEach(() => {
        findOne.mockReturnValue(undefined);
      });
      it('should throw an error', () => {
        const id = 1;
        expect(service.findOne(id)).rejects.toThrow(OrderNotFoundException);
      });
    });
  });

  describe('when updating an order', () => {
    describe('and it succeed', () => {
      const order = {
        ...orderMock,
        client: clientMock,
      };
      beforeEach(() => {
        update.mockReturnValue(order);
        findOne.mockReturnValue(order);
      });
      it('should return an order', () => {
        const id = 1;
        expect(service.update(id, order)).resolves.toEqual(order);
      });
    });
    describe('and it fails', () => {
      beforeEach(() => {
        update.mockReturnValue(undefined);
        findOne.mockReturnValue(undefined);
      });
      it('should throw an error', () => {
        const id = 1;
        expect(service.update(id, orderMock)).rejects.toThrow(OrderNotFoundException);
      });
    });
  });

  describe('when deleting an order', () => {
    describe('and it succeed', () => {
      const order = {
        ...orderMock,
        client: clientMock,
      };
      beforeEach(() => {
        findOne.mockReturnValue(order);
        deleteFunc.mockReturnValue(order);
      });
      it('should return an order', () => {
        const id = 1;
        expect(service.remove(id)).resolves.toEqual(order);
      });
    });

    describe('and it fails', () => {
      beforeEach(() => {
        findOne.mockReturnValue(undefined);
        deleteFunc.mockReturnValue(undefined);
      });
      it('should throw an error', () => {
        const id = 1;
        expect(service.remove(id)).rejects.toThrow(OrderNotFoundException);
      });
    });
  });
});
