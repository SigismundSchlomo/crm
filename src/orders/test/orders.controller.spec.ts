import { Test, TestingModule } from '@nestjs/testing';
import { OrdersController } from '../orders.controller';
import { OrdersService } from '../orders.service';
import { ClientsService } from '../../clients/clients.service';
import { clientMock } from '../../utils/mocks/client.mock';
import { orderMock } from '../../utils/mocks/order.mock';

describe('OrdersController', () => {
  let controller: OrdersController;
  const orderCreate = jest.fn();
  const orderGetAll = jest.fn();
  const orderFindOne = jest.fn();
  const orderUpdate = jest.fn();
  const orderRemove = jest.fn();
  const clientGetById = jest.fn();

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [OrdersController],
      providers: [
        {
          provide: OrdersService,
          useValue: {
            create: orderCreate,
            getAll: orderGetAll,
            findOne: orderFindOne,
            update: orderUpdate,
            remove: orderRemove,
          },
        },
        {
          provide: ClientsService,
          useValue: {
            getById: clientGetById,
          },
        },
      ],
    }).compile();

    controller = module.get<OrdersController>(OrdersController);
  });

  describe('when creating an Order', () => {
    const order = {
      ...orderMock,
      client: clientMock,
    };
    beforeEach(() => {
      clientGetById.mockReturnValue(clientMock);
      orderCreate.mockReturnValue(order);
    });
    it('should return an Order', () => {
      const clientId = 1;
      expect(controller.create(orderMock, clientId)).resolves.toEqual(order);
    });
  });

  describe('when getting all orders', () => {
    const orderArray = [
      {
        ...orderMock,
        client: clientMock,
      },
    ];
    beforeEach(() => {
      orderGetAll.mockReturnValue(orderArray);
    });
    it('should return an array of orders', () => {
      expect(controller.findAll()).toEqual(orderArray);
    });
  });

  describe('when getting one Order', () => {
    const order = {
      ...orderMock,
      client: clientMock,
    };
    beforeEach(() => {
      orderFindOne.mockReturnValue(order);
    });
    it('should return an order', () => {
      const id = 1;
      expect(controller.findOne(id)).toEqual(order);
    });
  });

  describe('when updating an order', () => {
    const order = {
      ...orderMock,
      client: clientMock,
    };
    beforeEach(() => {
      orderUpdate.mockReturnValue(order);
    });
    it('should return an order', () => {
      const id = 1;
      expect(controller.update(id, orderMock)).toEqual(order);
    });
  });

  describe('when removing an order', () => {
    const order = {
      ...orderMock,
      client: clientMock,
    };
    beforeEach(() => {
      orderRemove.mockReturnValue(order);
    });
    it('should return an order', () => {
      const id = 1;
      expect(controller.remove(id)).toEqual(order);
    });
  });
});
