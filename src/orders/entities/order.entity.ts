import { Column, Entity, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import Client from '../../clients/entities/client.entity';
import { Product } from '../../products/entities/product.entity';

@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'timestamptz' })
  public date: Date;

  @Column()
  public deliveryAddress: string;

  @Column({ type: 'text' })
  public additionalNote?: string;

  @Column()
  public totalPrice: number;

  @Column()
  public executed: boolean;

  @ManyToOne(() => Client, (client: Client) => client.orders)
  public client: Client;

  @ManyToMany(() => Product, (product: Product) => product.orders)
  @JoinTable()
  public orderedProducts: Product[];
}
