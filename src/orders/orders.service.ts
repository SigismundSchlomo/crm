import { Injectable } from '@nestjs/common';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Order } from './entities/order.entity';
import { Repository } from 'typeorm';
import Client from '../clients/entities/client.entity';
import { OrderNotFoundException } from './exception/orderNotFound.exception';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private orderRepository: Repository<Order>,
  ) {}

  async create(order: CreateOrderDto, client: Client) {
    const newOrder = await this.orderRepository.create({
      ...order,
      client: client,
    });
    if (newOrder) {
      await this.orderRepository.save(newOrder);
      return newOrder;
    }
    throw new Error('Unable to create an order');
  }

  async getAll() {
    const orders = await this.orderRepository.find({ relations: ['client'] });
    if (orders) return orders;
    throw new Error("Couldn't get a list of orders");
  }

  async findOne(id: number) {
    const order = await this.orderRepository.findOne(id, { relations: ['author'] });
    if (order) return order;
    throw new OrderNotFoundException(id);
  }

  async update(id: number, order: UpdateOrderDto) {
    await this.orderRepository.update(id, order);
    const updatedOrder = await this.orderRepository.findOne(id, { relations: ['author'] });
    if (updatedOrder) return updatedOrder;
    throw new OrderNotFoundException(id);
  }

  async remove(id: number) {
    const removedOrder = await this.orderRepository.findOne(id);
    if (removedOrder) {
      await this.orderRepository.remove(removedOrder);
      return removedOrder;
    }
    throw new OrderNotFoundException(id);
  }
}
