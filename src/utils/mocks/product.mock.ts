export const productMock = {
  photoUrl: 'some-cool-photos.com',
  leftNum: 10,
  description: 'Dome text about this cool product',
  price: 228,
  orders: [],
};
