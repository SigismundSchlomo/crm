import { Injectable } from '@nestjs/common';
import { CreateClientDto } from './dto/create-client.dto';
import { UpdateClientDto } from './dto/update-client.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Client } from './entities/client.entity';
import { Repository } from 'typeorm';
import { ClientNotFoundException } from './exceptions/ClientNotFound.exception';

@Injectable()
export class ClientsService {
  constructor(
    @InjectRepository(Client)
    private clientRepository: Repository<Client>,
  ) {}

  async create(createClientDto: CreateClientDto) {
    const newClient = await this.clientRepository.create({
      ...createClientDto,
    });
    await this.clientRepository.save(newClient);
    return newClient;
  }

  async findAll() {
    return await this.clientRepository.find({ relations: ['orders'] });
  }

  async getById(id: number) {
    const client = await this.clientRepository.findOne(id, { relations: ['orders'] });
    if (client) return client;
    throw new ClientNotFoundException(id);
  }

  async update(id: number, client: UpdateClientDto) {
    await this.clientRepository.update(id, client);
    const updatedClient = await this.clientRepository.findOne(id);
    if (updatedClient) return updatedClient;
    throw new ClientNotFoundException(id);
  }

  async remove(id: number) {
    const client = await this.clientRepository.findOne(id);
    if (client) {
      await this.clientRepository.delete(id);
      return client
    }
    throw new ClientNotFoundException(id);
  }
}
