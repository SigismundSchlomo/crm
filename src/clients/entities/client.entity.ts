import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Order } from '../../orders/entities/order.entity';

@Entity()
export class Client {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  fullName: string;

  @Column({ nullable: true })
  telephoneNumber?: number;

  @Column({ nullable: true })
  countryCode?: number;

  @Column({ nullable: true })
  email: string;

  @OneToMany(() => Order, (order: Order) => order.client)
  public orders: Order[];

}

export default Client;
