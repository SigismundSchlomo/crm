import { PartialType } from '@nestjs/mapped-types';
import { ClientDto } from './client.dto';
import { IsArray, IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';
import { Order } from '../../orders/entities/order.entity';

export class CreateClientDto extends PartialType(ClientDto) {
  @IsString()
  @IsNotEmpty()
  fullName: string;

  @IsString()
  @IsNotEmpty()
  email: string;

  @IsNumber()
  @IsNotEmpty()
  @IsOptional()
  telephoneNumber?: number;

  @IsNumber()
  @IsNotEmpty()
  @IsOptional()
  countryCode?: number;

  @IsArray()
  @IsNotEmpty()
  @IsOptional()
  orders: Order[];
}
