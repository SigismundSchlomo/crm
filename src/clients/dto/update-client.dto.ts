import { PartialType } from '@nestjs/mapped-types';
import { ClientDto } from './client.dto';
import { IsArray, IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';
import { Order } from '../../orders/entities/order.entity';

export class UpdateClientDto extends PartialType(ClientDto) {
  @IsString()
  @IsNotEmpty()
  @IsOptional()
  fullName?: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  email?: string;

  @IsNumber()
  @IsOptional()
  @IsNotEmpty()
  telephoneNumber?: number;

  @IsNumber()
  @IsNotEmpty()
  @IsOptional()
  countryCode?: number;

  @IsOptional()
  @IsNotEmpty()
  @IsArray()
  orders: Order[];
}
