import { Order } from '../../orders/entities/order.entity';

export class ClientDto {
  id: number;
  fullName: string;
  telephoneNumber?: number;
  countryCode?: number;
  email?: string;
  orders: Order[];
}
