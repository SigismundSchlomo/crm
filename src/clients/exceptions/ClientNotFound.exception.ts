import { HttpException, HttpStatus } from '@nestjs/common';

export class ClientNotFoundException extends HttpException {
  constructor(clientId: number) {
    super(`Client with id ${clientId} not Found`, HttpStatus.NOT_FOUND);
  }
}
