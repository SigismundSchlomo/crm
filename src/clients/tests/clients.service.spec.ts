import { ClientsService } from '../clients.service';
import { Test } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import Client from '../entities/client.entity';
import { ClientNotFoundException } from '../exceptions/ClientNotFound.exception';
import { clientMock } from '../../utils/mocks/client.mock';

describe('The Clients Service', () => {
let clientsService: ClientsService;
  const create = jest.fn();
  const save = jest.fn();
  const find = jest.fn();
  const findOne = jest.fn();
  const deleteFunc = jest.fn();
  const update = jest.fn();

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [
        ClientsService,
        {
          provide: getRepositoryToken(Client),
          useValue: {
            create,
            save,
            find,
            findOne,
            delete: deleteFunc,
            update,
          },
        },
      ],
    }).compile();
    clientsService = module.get(ClientsService);
  });

  describe('when creating a client', () => {
    beforeEach(() => {
      create.mockReturnValue(clientMock);
      save.mockReturnValue(clientMock);
    });
    it('should return a same client', () => {
      expect(clientsService.create(clientMock)).resolves.toEqual(clientMock);
    });
  });

  describe('when getting all clients', () => {
    const mockedArray = [clientMock];
    beforeEach(() => {
      find.mockReturnValue(mockedArray);
    });
    it('should return an array of clients', () => {
      expect(clientsService.findAll()).resolves.toEqual(mockedArray);
    });
  });

  describe('when getting client by id', () => {
    describe('and it succeed', () => {
      beforeEach(() => {
        findOne.mockReturnValue(clientMock);
      });
      const id = 1;
      it('should return a client', () => {
        expect(clientsService.getById(id)).resolves.toEqual(clientMock);
      });
    });
    describe('and id fails', () => {
      beforeEach(() => {
        findOne.mockReturnValue(undefined);
      });
      const id = 1;
      it('should throw an error', () => {
        expect(clientsService.getById(id)).rejects.toThrow(ClientNotFoundException);
      });
    });
  });

  describe('when updating the client', () => {
    describe('and it succeed', () => {
      beforeEach(() => {
        update.mockReturnValue(clientMock);
        findOne.mockReturnValue(clientMock);
      });
      it('should return updatedClient', () => {
        const id = 1;
        const updateDto = clientMock;
        expect(clientsService.update(id, updateDto)).resolves.toEqual(clientMock);
      });
    });
    describe('and it fails', () => {
      beforeEach(() => {
        update.mockReturnValue(undefined);
        findOne.mockReturnValue(undefined);
      });
      it('should throw an error', () => {
        const id = 1;
        const updateDto = { orders: [] };
        expect(clientsService.update(id, updateDto)).rejects.toThrow(ClientNotFoundException);
      });
    });
  });

  describe('when deleting a client', () => {
    describe('and it succeed', () => {
      beforeEach(() => {
        findOne.mockReturnValue(clientMock);
        deleteFunc.mockReturnValue(clientMock);
      });
      it('should return deleted client', () => {
        const id = 1;
        expect(clientsService.remove(id)).resolves.toEqual(clientMock);
      });
    });
    describe('and it fails', () => {
      beforeEach(() => {
        findOne.mockReturnValue(undefined);
        deleteFunc.mockReturnValue(undefined);
      });
      it('should throw an error', () => {
        const id = 1;
        expect(clientsService.remove(id)).rejects.toThrow(ClientNotFoundException);
      });
    });
  });
});
