import { ClientsService } from '../clients.service';
import { Test } from '@nestjs/testing';
import { ClientsController } from '../clients.controller';
import { clientMock } from '../../utils/mocks/client.mock';
import { UpdateClientDto } from '../dto/update-client.dto';

describe('The Clients Controller', () => {
  let clientsController: ClientsController;
  const create = jest.fn();
  const findAll = jest.fn();
  const getById = jest.fn();
  const update = jest.fn();
  const remove = jest.fn();

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [
        {
          provide: ClientsService,
          useValue: {
            create,
            findAll,
            getById,
            update,
            remove,
          },
        },
      ],
      controllers: [ClientsController],
    }).compile();
    clientsController = module.get(ClientsController);
  });

  describe('when create new client', () => {
    beforeEach(() => {
      create.mockReturnValue(clientMock);
    });
    it('should return a new client', () => {
      expect(clientsController.create(clientMock)).toEqual(clientMock);
    });
  });

  describe('when retrieving all clients', () => {
    const mockedArray = [clientMock];
    beforeEach(() => {
      findAll.mockReturnValue(mockedArray);
    });
    it('should return an array of clients', () => {
      expect(clientsController.findAll()).toEqual(mockedArray);
    });
  });

  describe('when retrieving one client', () => {
    beforeEach(() => {
      getById.mockReturnValue(clientMock);
    });
    it('should return a client', () => {
      const findOneParams = { id: 1 };
      expect(clientsController.findOne(findOneParams)).toEqual(clientMock);
    });
  });

  describe('when update a client', () => {
    beforeEach(() => {
      update.mockReturnValue(clientMock);
    });
    it('should return an updated client', () => {
      const id = 1;
      const updateDto: UpdateClientDto = clientMock;
      expect(clientsController.update(id, updateDto)).toEqual(clientMock);
    });
  });

  describe('when remove a client', () => {
    beforeEach(() => {
      remove.mockReturnValue(clientMock);
    });
    it('should return the deleted client', () => {
      const id = 1;
      expect(clientsController.remove(id)).toEqual(clientMock);
    });
  });
});
