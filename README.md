
# Description

Crm is a system to manage relations with clients. This project provides backend for such system

## Live demo

Go to api.kuznietsov.page to see project's page and try live swagger documentation

## Technological description
Project build with nest.js framework in typescript. Hosted with nginx and Postgresql is a database of choice. 
During development were used modern methodologies such as CI/CD with help of gitlab platform and agile planning.